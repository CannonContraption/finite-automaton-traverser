/*
  TRANSITION SYSTEM GENERATOR
  WRITTEN 2018-11-5 JIM READ

  TAKES AN INPUT FILE FOR AN AUTOMATON AND ALLOWS A USER TO EXPLORE IT
*/

/*
  file format:
  
  [terminal]
  label=?
  accepting=no

  [terminal]
  label=string
  accepting=yes/no

  [transition]
  from=label
  to=label
  label=string
*/

#include<iostream>
#include<fstream>
#include<vector>

using namespace std;

/*
  terminal
  
  Includes a label and whether it's accepting.
*/
struct terminal
{
  string label;
  bool   accepting;
};

/*
  transition
  
  Includes a connection between two different labels.
  
  The program doesn't strictly check if terminals are unique, but that hardly
  matters since the label is what they're referenced by, and so using the same
  label will simply always reference the same node.
*/
struct transition
{
  string label_from;
  string label_to;
  string transition_label;
};

/*
  Three lists of importance:
  the terminals that the program can use (and whether they're accepting)
  the transitions, where they come from where they go
  the stack, where visited transitions end up.
*/
vector<terminal>   terminals;
vector<transition> transitions;
vector<transition> stack;

/*
  show_terminals
  
  Shows all the terminals the program knows about, and whether they're accepting
  states.
*/
void show_terminals()
{
  for(
      int terminal_index = 0;
      terminal_index < terminals.size();
      terminal_index ++)
    {
      cout<<
        "TERMINAL:"<<
        terminals[terminal_index].label<<
        (terminals[terminal_index].accepting ? " is an accepting state." : ".")<<
        endl;
    }
}

/*
  show_terminals
  
  Shows every possible transition between nodes.
*/
void show_transitions()
{
  for(
      int transition_index = 0;
      transition_index < transitions.size();
      transition_index ++)
    {
      cout<<
        transitions[transition_index].transition_label <<
        ": from '" <<
        transitions[transition_index].label_from <<
        "' to '" <<
        transitions[transition_index].label_to <<
        "'." <<
        endl;
    }
}

/*
  show_stack
  
  shows all transition steps taken so far.
*/
void show_stack()
{
  for(
      int transition_index = 0;
      transition_index < transitions.size();
      transition_index ++)
    {
      cout<<
        stack[transition_index].transition_label <<
        ": from '" <<
        stack[transition_index].label_from <<
        "' to '" <<
        stack[transition_index].label_to <<
        "'." <<
        endl;
    }
}

/*
  Pointer to the current state.
  
  Used to determine the current label and check if we can quit yet.
*/
terminal * currentstate;

/*
  runautomata
  
  Starts an interactive automata run.
  Hands control to the user behind stdin and lets them play.
*/
int runautomata()
{
  bool valid_next_step;
  string next_step = "?";
  do
    {
      valid_next_step = false;
      cout<<"Next step? Options:"<<endl;
      for(
          int transition_index = 0;
          transition_index < transitions.size();
          transition_index ++)
        {
          if(transitions[transition_index].label_from == currentstate->label)
            {
              cout<<transitions[transition_index].transition_label<<endl;
            }
        }
      if(currentstate -> accepting)
        cout<<"\"quit\" quits."<<endl;
      else
        cout<<"Get to an accepting state to quit."<<endl;
      cin>>next_step;
      for(
          int transition_index = 0;
          transition_index < transitions.size();
          transition_index ++)
        {
          if(transitions[transition_index].label_from == currentstate->label)
            {
              valid_next_step = true;
              stack.emplace_back(transitions[transition_index]);
              break;
            }
        }
      if(valid_next_step)
        {
          for(
              int terminal_index = 0;
              terminal_index < terminals.size();
              terminal_index ++)
            {
              if(terminals[terminal_index].label == next_step)
                {
                  currentstate = &terminals[terminal_index];
                  break;
                }
            }
          if(currentstate -> label != next_step)
            {
              cerr<<"[1;31mSyntax error in input file![m"<<endl;
              cerr<<endl;
              cerr<<"Transition exists, terminal could not be found!"<<endl;
              cerr<<endl<<endl;
              return -1;
            }
        }
      else
        {
          cerr<<"Not a valid transition!"<<endl;
        }
    }
  while(next_step != "?" && !currentstate -> accepting);
  show_stack();
  return 0;
}

/*
  readconnections
  
  Reads the transition system from a file.
  Should be formatted like the one in the comments at the top of the file.
*/
void readconnections(
    string transitions_file_name)
{
  fstream transitionsfile;
  transitionsfile.open(
      transitions_file_name,
      fstream::in);

  /*
    Line read mode.
    0: ignore next line.
    1: line is for a terminal.
    2: line is for a transition.
    
    Each mode is designed to relinquish control to 0 upon seeing the last
    expected term in the list. This means that mode 0 can continue to find new
    sections, be it in the form of terminals or transitions.
  */
  int mode = 0;
  terminal   terminal_list_builder;
  transition transition_list_builder;
  string     currentline;
  string     key;
  string     value;
  bool       isvalue = false;
  while(
      !transitionsfile.eof() &&
      !transitionsfile.fail())
    {
      value        = "";
      key          = "";
      isvalue      = false;
      currentline = "";

      getline(
          transitionsfile,
          currentline);

      if(mode == 0)
        {
          if(currentline == "[terminal]")
            mode = 1;
          else if(currentline == "[transition]")
            mode = 2;
        }
      else if(mode == 1)
        {
          for(char c : currentline)
            {
              if(isvalue == true)
                value += c;
              else if(c == '=')
                isvalue = true;
              else
                key += c;
            }
          if(key == "label")
            {
              terminal_list_builder.label = value;
            }
          if(key == "accepting")
            {
              terminal_list_builder.accepting = true ? "yes" : "no";
              terminals.emplace_back(terminal_list_builder);
              mode = 0;
            }
        }
      else if(mode >= 2)
        {
          for(char c : currentline)
            {
              if(isvalue == true)
                value += c;
              else if(c == '=')
                isvalue = true;
              else
                key += c;
            }
          if(key == "from")
            {
              transition_list_builder.label_from = value;
            }
          else if(key == "to")
            {
              transition_list_builder.label_to = value;
            }
          else if(key == "label")
            {
              transition_list_builder.transition_label = value;
              transitions.emplace_back(
                  transition_list_builder);
              mode = 0;
            }
        }
    }
}

/*
  At this point, all we need do is kick things off. The program is complete
  enough to read transitions from a file and then interactively walk the user
  through all the states contained within, assuming it is a properly formatted
  transitions file.
*/
int main(
    int    argc,
    char * argv[])
{
  string filename;
  if(argc < 2)
    {
      cerr<<
        "Please enter the name of a transitions file as the first argument." <<
        endl;
      return 1;
    }
  filename = argv[1];
  readconnections(filename);
  show_terminals();
  show_transitions();
  currentstate = &terminals[0];
  runautomata();
  return 0;
}
